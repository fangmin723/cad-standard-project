# CAD-Standard-Project

#### 介绍


#### 软件架构
本架构包含CAD二次开发框架为：.Net3.5、.Net4.0、.Net4.5、.Net4.6、.Net4.7、.Net4.8；
为CAD二次开发进行一站式配置！

#### 贡献
本仓库所有源码，皆源自[《cad.net 从.net framework移植到.net standard》](https://www.cnblogs.com/JJBox/p/12677496.html)
